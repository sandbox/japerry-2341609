<?php
/**
 * @file
 * drupalcon_commerce_reports.features.inc
 */

/**
 * Implements hook_views_api().
 */
function drupalcon_commerce_reports_views_api() {
  return array("version" => "3.0");
}
