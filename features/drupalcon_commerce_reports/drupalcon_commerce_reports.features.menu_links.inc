<?php
/**
 * @file
 * drupalcon_commerce_reports.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function drupalcon_commerce_reports_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management:admin/commerce/reports
  $menu_links['management:admin/commerce/reports'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/commerce/reports',
    'router_path' => 'admin/commerce/reports',
    'link_title' => 'Reports',
    'options' => array(
      'attributes' => array(
        'title' => 'View reports for your store.',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '-20',
    'parent_path' => 'admin/commerce',
  );
  // Exported menu link: management:admin/commerce/reports/all-sales
  $menu_links['management:admin/commerce/reports/all-sales'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/commerce/reports/all-sales',
    'router_path' => 'admin/commerce/reports/all-sales',
    'link_title' => 'Sales - All',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'admin/commerce/reports',
  );
  // Exported menu link: management:admin/commerce/reports/line-items-non-taxable
  $menu_links['management:admin/commerce/reports/line-items-non-taxable'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/commerce/reports/line-items-non-taxable',
    'router_path' => 'admin/commerce/reports/line-items-non-taxable',
    'link_title' => 'Sales - Non-taxable',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'admin/commerce/reports',
  );
  // Exported menu link: management:admin/commerce/reports/line-items-taxable
  $menu_links['management:admin/commerce/reports/line-items-taxable'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/commerce/reports/line-items-taxable',
    'router_path' => 'admin/commerce/reports',
    'link_title' => 'Sales - Taxable',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'admin/commerce/reports',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Reports');
  t('Sales - All');
  t('Sales - Non-taxable');
  t('Sales - Taxable');


  return $menu_links;
}
