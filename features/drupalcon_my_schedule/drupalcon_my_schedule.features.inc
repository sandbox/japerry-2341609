<?php
/**
 * @file
 * drupalcon_my_schedule.features.inc
 */

/**
 * Implements hook_views_api().
 */
function drupalcon_my_schedule_views_api() {
  return array("version" => "3.0");
}
