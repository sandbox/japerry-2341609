<?php
/**
 * @file
 * drupalcon_my_schedule.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function drupalcon_my_schedule_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'personal_schedule';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Personal Schedule';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'My Schedule';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_session_slot',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Flags: session_schedule */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'session_schedule';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_session_slot_target_id']['id'] = 'field_session_slot_target_id';
  $handler->display->display_options['relationships']['field_session_slot_target_id']['table'] = 'field_data_field_session_slot';
  $handler->display->display_options['relationships']['field_session_slot_target_id']['field'] = 'field_session_slot_target_id';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Time slot */
  $handler->display->display_options['fields']['field_session_slot']['id'] = 'field_session_slot';
  $handler->display->display_options['fields']['field_session_slot']['table'] = 'field_data_field_session_slot';
  $handler->display->display_options['fields']['field_session_slot']['field'] = 'field_session_slot';
  $handler->display->display_options['fields']['field_session_slot']['label'] = '';
  $handler->display->display_options['fields']['field_session_slot']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_session_slot']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_session_slot']['settings'] = array(
    'link' => 0,
  );
  /* Field: Content: Room */
  $handler->display->display_options['fields']['field_session_room']['id'] = 'field_session_room';
  $handler->display->display_options['fields']['field_session_room']['table'] = 'field_data_field_session_room';
  $handler->display->display_options['fields']['field_session_room']['field'] = 'field_session_room';
  $handler->display->display_options['fields']['field_session_room']['label'] = '';
  $handler->display->display_options['fields']['field_session_room']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_session_room']['settings'] = array(
    'link' => 0,
  );
  /* Sort criterion: Content: Date and time (field_slot_datetime:value2) */
  $handler->display->display_options['sorts']['field_slot_datetime_value2']['id'] = 'field_slot_datetime_value2';
  $handler->display->display_options['sorts']['field_slot_datetime_value2']['table'] = 'field_data_field_slot_datetime';
  $handler->display->display_options['sorts']['field_slot_datetime_value2']['field'] = 'field_slot_datetime_value2';
  $handler->display->display_options['sorts']['field_slot_datetime_value2']['relationship'] = 'field_session_slot_target_id';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'schedule_item' => 'schedule_item',
    'session' => 'session',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'user/%/schedule';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'My Schedule';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $translatables['personal_schedule'] = array(
    t('Master'),
    t('My Schedule'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('flag'),
    t('Content entity referenced from field_session_slot'),
    t('Page'),
  );
  $export['personal_schedule'] = $view;

  return $export;
}
