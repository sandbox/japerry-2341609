<?php
/**
 * @file
 * drupalcon_sponsors.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function drupalcon_sponsors_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'sponsors';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Sponsors';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Sponsors';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_sponsor_level',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Field: Website */
  $handler->display->display_options['fields']['field_website']['id'] = 'field_website';
  $handler->display->display_options['fields']['field_website']['table'] = 'field_data_field_website';
  $handler->display->display_options['fields']['field_website']['field'] = 'field_website';
  $handler->display->display_options['fields']['field_website']['label'] = '';
  $handler->display->display_options['fields']['field_website']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_website']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_website']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_website']['type'] = 'link_url';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = '[field_website]';
  $handler->display->display_options['fields']['title']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Sponsor level */
  $handler->display->display_options['fields']['field_sponsor_level']['id'] = 'field_sponsor_level';
  $handler->display->display_options['fields']['field_sponsor_level']['table'] = 'field_data_field_sponsor_level';
  $handler->display->display_options['fields']['field_sponsor_level']['field'] = 'field_sponsor_level';
  $handler->display->display_options['fields']['field_sponsor_level']['label'] = '';
  $handler->display->display_options['fields']['field_sponsor_level']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_sponsor_level']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_sponsor_level']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['field_sponsor_level']['delta_offset'] = '0';
  /* Field: Content: Sponsor logo */
  $handler->display->display_options['fields']['field_sponsor_logo']['id'] = 'field_sponsor_logo';
  $handler->display->display_options['fields']['field_sponsor_logo']['table'] = 'field_data_field_sponsor_logo';
  $handler->display->display_options['fields']['field_sponsor_logo']['field'] = 'field_sponsor_logo';
  $handler->display->display_options['fields']['field_sponsor_logo']['label'] = '';
  $handler->display->display_options['fields']['field_sponsor_logo']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_sponsor_logo']['alter']['path'] = '[field_website]';
  $handler->display->display_options['fields']['field_sponsor_logo']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['field_sponsor_logo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_sponsor_logo']['empty'] = '[title]';
  $handler->display->display_options['fields']['field_sponsor_logo']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_sponsor_logo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_sponsor_logo']['settings'] = array(
    'image_style' => 'medium',
    'image_link' => '',
  );
  /* Sort criterion: Content: Sponsor level (field_sponsor_level) */
  $handler->display->display_options['sorts']['field_sponsor_level_value']['id'] = 'field_sponsor_level_value';
  $handler->display->display_options['sorts']['field_sponsor_level_value']['table'] = 'field_data_field_sponsor_level';
  $handler->display->display_options['sorts']['field_sponsor_level_value']['field'] = 'field_sponsor_level_value';
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'sponsors' => 'sponsors',
  );

  /* Display: Sponsors Page */
  $handler = $view->new_display('page', 'Sponsors Page', 'page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Thank you to our sponsors';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_sponsor_level',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['style_options']['row_class'] = 'views-row--[field_sponsor_level]';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Field: Website */
  $handler->display->display_options['fields']['field_website']['id'] = 'field_website';
  $handler->display->display_options['fields']['field_website']['table'] = 'field_data_field_website';
  $handler->display->display_options['fields']['field_website']['field'] = 'field_website';
  $handler->display->display_options['fields']['field_website']['label'] = '';
  $handler->display->display_options['fields']['field_website']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_website']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_website']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_website']['type'] = 'link_url';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = '[field_website]';
  $handler->display->display_options['fields']['title']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Sponsor level */
  $handler->display->display_options['fields']['field_sponsor_level']['id'] = 'field_sponsor_level';
  $handler->display->display_options['fields']['field_sponsor_level']['table'] = 'field_data_field_sponsor_level';
  $handler->display->display_options['fields']['field_sponsor_level']['field'] = 'field_sponsor_level';
  $handler->display->display_options['fields']['field_sponsor_level']['label'] = '';
  $handler->display->display_options['fields']['field_sponsor_level']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_sponsor_level']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_sponsor_level']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['field_sponsor_level']['delta_offset'] = '0';
  /* Field: Content: Sponsor logo */
  $handler->display->display_options['fields']['field_sponsor_logo']['id'] = 'field_sponsor_logo';
  $handler->display->display_options['fields']['field_sponsor_logo']['table'] = 'field_data_field_sponsor_logo';
  $handler->display->display_options['fields']['field_sponsor_logo']['field'] = 'field_sponsor_logo';
  $handler->display->display_options['fields']['field_sponsor_logo']['label'] = '';
  $handler->display->display_options['fields']['field_sponsor_logo']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_sponsor_logo']['alter']['path'] = '[field_website]';
  $handler->display->display_options['fields']['field_sponsor_logo']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['field_sponsor_logo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_sponsor_logo']['empty'] = '[title]';
  $handler->display->display_options['fields']['field_sponsor_logo']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_sponsor_logo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_sponsor_logo']['settings'] = array(
    'image_style' => 'medium',
    'image_link' => '',
  );
  /* Field: Content: Sponsor module level */
  $handler->display->display_options['fields']['field_sponsor_module_level']['id'] = 'field_sponsor_module_level';
  $handler->display->display_options['fields']['field_sponsor_module_level']['table'] = 'field_data_field_sponsor_module_level';
  $handler->display->display_options['fields']['field_sponsor_module_level']['field'] = 'field_sponsor_module_level';
  $handler->display->display_options['fields']['field_sponsor_module_level']['label'] = '';
  $handler->display->display_options['fields']['field_sponsor_module_level']['element_label_colon'] = FALSE;
  $handler->display->display_options['path'] = 'sponsors';

  /* Display: Diamond Sponsors */
  $handler = $view->new_display('block', 'Diamond Sponsors', 'block_diamond');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Diamond';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'sponsors' => 'sponsors',
  );
  /* Filter criterion: Content: Sponsor level (field_sponsor_level) */
  $handler->display->display_options['filters']['field_sponsor_level_value']['id'] = 'field_sponsor_level_value';
  $handler->display->display_options['filters']['field_sponsor_level_value']['table'] = 'field_data_field_sponsor_level';
  $handler->display->display_options['filters']['field_sponsor_level_value']['field'] = 'field_sponsor_level_value';
  $handler->display->display_options['filters']['field_sponsor_level_value']['value'] = array(
    1 => '1',
  );
  $handler->display->display_options['block_description'] = 'Diamond Sponsor Block';

  /* Display: Platinum Sponsors */
  $handler = $view->new_display('block', 'Platinum Sponsors', 'block_platinum');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Platinum';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'sponsors' => 'sponsors',
  );
  /* Filter criterion: Content: Sponsor level (field_sponsor_level) */
  $handler->display->display_options['filters']['field_sponsor_level_value']['id'] = 'field_sponsor_level_value';
  $handler->display->display_options['filters']['field_sponsor_level_value']['table'] = 'field_data_field_sponsor_level';
  $handler->display->display_options['filters']['field_sponsor_level_value']['field'] = 'field_sponsor_level_value';
  $handler->display->display_options['filters']['field_sponsor_level_value']['value'] = array(
    2 => '2',
  );
  $handler->display->display_options['block_description'] = 'Platinum Sponsors';
  $export['sponsors'] = $view;

  return $export;
}
