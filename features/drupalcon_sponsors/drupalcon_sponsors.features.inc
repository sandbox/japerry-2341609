<?php
/**
 * @file
 * drupalcon_sponsors.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function drupalcon_sponsors_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function drupalcon_sponsors_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function drupalcon_sponsors_node_info() {
  $items = array(
    'sponsors' => array(
      'name' => t('Sponsors'),
      'base' => 'node_content',
      'description' => t('Track DrupalCon sponsor information and assets in this content type.'),
      'has_title' => '1',
      'title_label' => t('Sponsor name'),
      'help' => '',
    ),
  );
  return $items;
}
