<?php
/**
 * @file
 * Code for the COD Session Evaluations feature.
 */

include_once 'drupalcon_session_evaluations.features.inc';

/**
* Implements hook_form_alter().
*/
function drupalcon_session_evaluations_form_session_evaluation_node_form_alter(&$form, &$form_state, $form_id) {
  if (!empty($form['field_eval_session']['und']['#default_value'])) {
    $session_nid = $form['field_eval_session']['und']['#default_value'];
    $form['field_eval_session']['#disabled'] = TRUE;
    // If the user has already reviewed the referenced session, don't let her
    // submit another session evaluation.
    if (drupalcon_session_evaluations_user_reviewed_session($form['uid']['#value'], $session_nid[0])) {
      $form['#access'] = FALSE;
      drupal_set_message(t('You have already evaluated this session. You can only submit one evaluation per session.'));
      drupal_goto('node/'. $session_nid[0]);
    }
  }
}

/**
* Implements hook_node_view().
*/
function drupalcon_session_evaluations_node_view($node, $view_mode, $langcode) {
  //sanity check -- we should only be posting on the session nodes. Return if its any other node or listing
  //todo -- probably want to be able add the link for a listing of nodes, but we can't at the moment
  //        since entities aren't loaded on anything but the full view
  if($node->type != 'session' || $view_mode != 'full') {
    return;
  }
  global $user;
  $account = $user;
  $bundle = 'session';

  //lets check first that the session was accepted AND its the event has started
  $time_slot = $node->field_session_slot['und'][0]['entity'];
  //how could you evaluate a presentation if it wasn't ever scheduled?
  if(empty($time_slot)) {
    return;
  }

  //how could you evaluate a presenatation if it never was accepted OR it hasn't occured yet?
  //note -- TZ handling could cause issues here, but as long as you don't have multiple timezones on one conference, it should be good
  $time = new DateTime(date('Y-m-d H:i:s'), new DateTimeZone($time_slot->field_slot_datetime['und'][0]['timezone']));
  $timezoneOffset = $time->format('Z');
  $endTime = strtotime($time_slot->field_slot_datetime['und'][0]['value2']) + $timezoneOffset;
  if(!($node->field_accepted['und'][0]['value']) || $endTime > time()) {
    return;
  }
  // Hide the "Provide feedback on this session" links if the user has already
  // submitted a survey about this session.
  if (!drupalcon_session_evaluations_user_reviewed_session($account->uid, $node->nid) && $node->type == $bundle) {
    if (user_is_logged_in()) {
      $links = array(
        'feedback' => array(
          'title' => variable_get('drupalcon_session_evaluations_feedback_text', t('Provide feedback on this session')),
          'href' => 'node/add/session-evaluation',
          'query' => array('nid' => $node->nid, 'destination' => $_GET['q']),
        ),
      );
    }
    else {
      $links = array(
        'feedback' => array(
          'title' => t('Log in to evaluate this session'),
          'href' => 'user/login',
          'query' => array('destination' => $_GET['q']),
        ),
      );
    }

    $node->content['links']['drupalcon_session_evaluations_feedback'] = array(
      '#theme' => 'links__node__drupalcon_session_evaluations_feedback',
      '#links' => $links,
      '#attributes' => array('class' => array('links', 'inline')),
    );
  }
  else {
    drupal_set_message(t('Thanks for providing feedback on this session.'));
  }
}

/**
* Implements hook_node_validate().
*/
function drupalcon_session_evaluations_node_validate($node, $form, &$form_state) {
  if ($node->type == 'session_evaluation') {
    // Check if user has already submitted an evaluation for this session.
    if (drupalcon_session_evaluations_user_reviewed_session($form['uid']['#value'], $form['field_eval_session']['und']['#value'])) {
      form_set_error('field_eval_session', t('You have already evaluated this session. You can only submit one evaluation per session.'));
    }
  }
}

/**
 * Implements hook_field_widget_form_alter().
 */
function drupalcon_session_evaluations_field_widget_form_alter(&$element, &$form_state, $context) {
  $field_name = 'field_eval_session';
  // Set a default value based on the nid.
  if (isset($element['#field_name']) && $element['#field_name'] == $field_name && isset($_GET['nid'])) {
    $element['#default_value'] = $_GET['nid'];
  }
}

/**
* Helper function to determine whether a user has already reviewed
* a particular session.
*/
function drupalcon_session_evaluations_user_reviewed_session($uid, $nid) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->propertyCondition('status', 1)
    ->propertyCondition('type', 'session_evaluation')
    ->propertyCondition('uid', $uid)
    ->fieldCondition('field_eval_session', 'target_id', $nid);
  $result = $query->execute();
  return !empty($result['node']);
}

