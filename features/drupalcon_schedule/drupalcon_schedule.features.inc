<?php
/**
 * @file
 * drupalcon_schedule.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function drupalcon_schedule_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function drupalcon_schedule_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_date_format_types().
 */
function drupalcon_schedule_date_format_types() {
  $format_types = array();
  // Exported date format type: time_only
  $format_types['time_only'] = 'time only';
  return $format_types;
}
