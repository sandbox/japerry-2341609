<?php
/**
 * @file
 * drupalcon_groundswell_reporting.features.inc
 */

/**
 * Implements hook_views_api().
 */
function drupalcon_groundswell_reporting_views_api() {
  return array("version" => "3.0");
}
