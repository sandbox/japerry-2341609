<?php
/**
 * @file
 * drupalcon_attendees.features.inc
 */

/**
 * Implements hook_views_api().
 */
function drupalcon_attendees_views_api() {
  return array("version" => "3.0");
}
