<?php
/**
 * @file
 * drupalcon_attendees.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function drupalcon_attendees_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'attendees';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'registration';
  $view->human_name = 'Attendees';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Attendees';
  $handler->display->display_options['css_class'] = 'attendees-custom-overrides ';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Registration: User */
  $handler->display->display_options['relationships']['user_uid']['id'] = 'user_uid';
  $handler->display->display_options['relationships']['user_uid']['table'] = 'registration';
  $handler->display->display_options['relationships']['user_uid']['field'] = 'user_uid';
  /* Relationship: Registration: State entity */
  $handler->display->display_options['relationships']['state']['id'] = 'state';
  $handler->display->display_options['relationships']['state']['table'] = 'registration';
  $handler->display->display_options['relationships']['state']['field'] = 'state';
  /* Field: User: Picture */
  $handler->display->display_options['fields']['picture']['id'] = 'picture';
  $handler->display->display_options['fields']['picture']['table'] = 'users';
  $handler->display->display_options['fields']['picture']['field'] = 'picture';
  $handler->display->display_options['fields']['picture']['relationship'] = 'user_uid';
  $handler->display->display_options['fields']['picture']['label'] = '';
  $handler->display->display_options['fields']['picture']['element_label_colon'] = FALSE;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'user_uid';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['name']['overwrite_anonymous'] = TRUE;
  /* Field: Registration: First name */
  $handler->display->display_options['fields']['field_badge_first_name']['id'] = 'field_badge_first_name';
  $handler->display->display_options['fields']['field_badge_first_name']['table'] = 'field_data_field_badge_first_name';
  $handler->display->display_options['fields']['field_badge_first_name']['field'] = 'field_badge_first_name';
  $handler->display->display_options['fields']['field_badge_first_name']['exclude'] = TRUE;
  /* Field: Registration: Last name */
  $handler->display->display_options['fields']['field_badge_last_name']['id'] = 'field_badge_last_name';
  $handler->display->display_options['fields']['field_badge_last_name']['table'] = 'field_data_field_badge_last_name';
  $handler->display->display_options['fields']['field_badge_last_name']['field'] = 'field_badge_last_name';
  $handler->display->display_options['fields']['field_badge_last_name']['exclude'] = TRUE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[field_badge_first_name] [field_badge_last_name]';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Field: Registration: Job title */
  $handler->display->display_options['fields']['field_badge_job_title']['id'] = 'field_badge_job_title';
  $handler->display->display_options['fields']['field_badge_job_title']['table'] = 'field_data_field_badge_job_title';
  $handler->display->display_options['fields']['field_badge_job_title']['field'] = 'field_badge_job_title';
  $handler->display->display_options['fields']['field_badge_job_title']['label'] = '';
  $handler->display->display_options['fields']['field_badge_job_title']['element_label_colon'] = FALSE;
  /* Field: Registration: Organization */
  $handler->display->display_options['fields']['field_badge_org']['id'] = 'field_badge_org';
  $handler->display->display_options['fields']['field_badge_org']['table'] = 'field_data_field_badge_org';
  $handler->display->display_options['fields']['field_badge_org']['field'] = 'field_badge_org';
  $handler->display->display_options['fields']['field_badge_org']['label'] = '';
  $handler->display->display_options['fields']['field_badge_org']['element_label_colon'] = FALSE;
  /* Field: Registration: Country */
  $handler->display->display_options['fields']['field_badge_country']['id'] = 'field_badge_country';
  $handler->display->display_options['fields']['field_badge_country']['table'] = 'field_data_field_badge_country';
  $handler->display->display_options['fields']['field_badge_country']['field'] = 'field_badge_country';
  $handler->display->display_options['fields']['field_badge_country']['label'] = '';
  $handler->display->display_options['fields']['field_badge_country']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_badge_country']['element_label_colon'] = FALSE;
  /* Sort criterion: User: User picture (field_user_picture:url) */
  $handler->display->display_options['sorts']['field_user_picture_url']['id'] = 'field_user_picture_url';
  $handler->display->display_options['sorts']['field_user_picture_url']['table'] = 'field_data_field_user_picture';
  $handler->display->display_options['sorts']['field_user_picture_url']['field'] = 'field_user_picture_url';
  $handler->display->display_options['sorts']['field_user_picture_url']['relationship'] = 'user_uid';
  $handler->display->display_options['sorts']['field_user_picture_url']['order'] = 'DESC';
  /* Filter criterion: Registration: State entity */
  $handler->display->display_options['filters']['state']['id'] = 'state';
  $handler->display->display_options['filters']['state']['table'] = 'registration';
  $handler->display->display_options['filters']['state']['field'] = 'state';
  $handler->display->display_options['filters']['state']['value'] = array(
    'complete' => 'complete',
  );
  $handler->display->display_options['filters']['state']['group'] = 1;
  /* Filter criterion: Registration: Registration type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'registration';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'conference_registration' => 'conference_registration',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Registration: First name (field_badge_first_name) */
  $handler->display->display_options['filters']['field_badge_first_name_value']['id'] = 'field_badge_first_name_value';
  $handler->display->display_options['filters']['field_badge_first_name_value']['table'] = 'field_data_field_badge_first_name';
  $handler->display->display_options['filters']['field_badge_first_name_value']['field'] = 'field_badge_first_name_value';
  $handler->display->display_options['filters']['field_badge_first_name_value']['group'] = 1;
  $handler->display->display_options['filters']['field_badge_first_name_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_badge_first_name_value']['expose']['operator_id'] = 'field_badge_first_name_value_op';
  $handler->display->display_options['filters']['field_badge_first_name_value']['expose']['label'] = 'First name';
  $handler->display->display_options['filters']['field_badge_first_name_value']['expose']['operator'] = 'field_badge_first_name_value_op';
  $handler->display->display_options['filters']['field_badge_first_name_value']['expose']['identifier'] = 'field_badge_first_name_value';
  $handler->display->display_options['filters']['field_badge_first_name_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    3 => 0,
  );
  /* Filter criterion: Registration: Last name (field_badge_last_name) */
  $handler->display->display_options['filters']['field_badge_last_name_value']['id'] = 'field_badge_last_name_value';
  $handler->display->display_options['filters']['field_badge_last_name_value']['table'] = 'field_data_field_badge_last_name';
  $handler->display->display_options['filters']['field_badge_last_name_value']['field'] = 'field_badge_last_name_value';
  $handler->display->display_options['filters']['field_badge_last_name_value']['group'] = 1;
  $handler->display->display_options['filters']['field_badge_last_name_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_badge_last_name_value']['expose']['operator_id'] = 'field_badge_last_name_value_op';
  $handler->display->display_options['filters']['field_badge_last_name_value']['expose']['label'] = 'Last name';
  $handler->display->display_options['filters']['field_badge_last_name_value']['expose']['operator'] = 'field_badge_last_name_value_op';
  $handler->display->display_options['filters']['field_badge_last_name_value']['expose']['identifier'] = 'field_badge_last_name_value';
  $handler->display->display_options['filters']['field_badge_last_name_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    3 => 0,
  );
  /* Filter criterion: Registration: Organization (field_badge_org) */
  $handler->display->display_options['filters']['field_badge_org_value']['id'] = 'field_badge_org_value';
  $handler->display->display_options['filters']['field_badge_org_value']['table'] = 'field_data_field_badge_org';
  $handler->display->display_options['filters']['field_badge_org_value']['field'] = 'field_badge_org_value';
  $handler->display->display_options['filters']['field_badge_org_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_badge_org_value']['group'] = 1;
  $handler->display->display_options['filters']['field_badge_org_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_badge_org_value']['expose']['operator_id'] = 'field_badge_org_value_op';
  $handler->display->display_options['filters']['field_badge_org_value']['expose']['label'] = 'Organization';
  $handler->display->display_options['filters']['field_badge_org_value']['expose']['operator'] = 'field_badge_org_value_op';
  $handler->display->display_options['filters']['field_badge_org_value']['expose']['identifier'] = 'field_badge_org_value';
  $handler->display->display_options['filters']['field_badge_org_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    3 => 0,
  );
  /* Filter criterion: User: Name */
  $handler->display->display_options['filters']['uid']['id'] = 'uid';
  $handler->display->display_options['filters']['uid']['table'] = 'users';
  $handler->display->display_options['filters']['uid']['field'] = 'uid';
  $handler->display->display_options['filters']['uid']['relationship'] = 'user_uid';
  $handler->display->display_options['filters']['uid']['value'] = '';
  $handler->display->display_options['filters']['uid']['group'] = 1;
  $handler->display->display_options['filters']['uid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['uid']['expose']['operator_id'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['label'] = 'Username';
  $handler->display->display_options['filters']['uid']['expose']['operator'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['identifier'] = 'uid';
  $handler->display->display_options['filters']['uid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    13 => 0,
    18 => 0,
    23 => 0,
    28 => 0,
  );
  /* Filter criterion: Registration: Country (field_badge_country) */
  $handler->display->display_options['filters']['field_badge_country_value']['id'] = 'field_badge_country_value';
  $handler->display->display_options['filters']['field_badge_country_value']['table'] = 'field_data_field_badge_country';
  $handler->display->display_options['filters']['field_badge_country_value']['field'] = 'field_badge_country_value';
  $handler->display->display_options['filters']['field_badge_country_value']['group'] = 1;
  $handler->display->display_options['filters']['field_badge_country_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_badge_country_value']['expose']['operator_id'] = 'field_badge_country_value_op';
  $handler->display->display_options['filters']['field_badge_country_value']['expose']['label'] = 'Country';
  $handler->display->display_options['filters']['field_badge_country_value']['expose']['operator'] = 'field_badge_country_value_op';
  $handler->display->display_options['filters']['field_badge_country_value']['expose']['identifier'] = 'field_badge_country_value';
  $handler->display->display_options['filters']['field_badge_country_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    3 => 0,
  );
  /* Filter criterion: Registration: Show my user profile on the attendee page (field_include_my_profile) */
  $handler->display->display_options['filters']['field_include_my_profile_value']['id'] = 'field_include_my_profile_value';
  $handler->display->display_options['filters']['field_include_my_profile_value']['table'] = 'field_data_field_include_my_profile';
  $handler->display->display_options['filters']['field_include_my_profile_value']['field'] = 'field_include_my_profile_value';
  $handler->display->display_options['filters']['field_include_my_profile_value']['value'] = array(
    'Yes' => 'Yes',
  );
  $handler->display->display_options['filters']['field_include_my_profile_value']['group'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'attendees';
  $export['attendees'] = $view;

  return $export;
}
