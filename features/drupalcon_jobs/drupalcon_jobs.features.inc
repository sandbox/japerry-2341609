<?php
/**
 * @file
 * drupalcon_jobs.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function drupalcon_jobs_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function drupalcon_jobs_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function drupalcon_jobs_node_info() {
  $items = array(
    'job_board' => array(
      'name' => t('Job'),
      'base' => 'node_content',
      'description' => t('Sponsor submitted jobs. '),
      'has_title' => '1',
      'title_label' => t('Job title'),
      'help' => '',
    ),
  );
  return $items;
}
