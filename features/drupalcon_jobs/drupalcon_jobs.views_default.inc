<?php
/**
 * @file
 * drupalcon_jobs.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function drupalcon_jobs_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'job_board';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Job Board';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Job Board';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'title' => 'title',
    'field_job_organization' => 'field_job_organization',
  );
  $handler->display->display_options['row_options']['separator'] = '|';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'strong';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Organization */
  $handler->display->display_options['fields']['field_job_organization']['id'] = 'field_job_organization';
  $handler->display->display_options['fields']['field_job_organization']['table'] = 'field_data_field_job_organization';
  $handler->display->display_options['fields']['field_job_organization']['field'] = 'field_job_organization';
  $handler->display->display_options['fields']['field_job_organization']['label'] = '';
  $handler->display->display_options['fields']['field_job_organization']['element_type'] = 'strong';
  $handler->display->display_options['fields']['field_job_organization']['element_label_colon'] = FALSE;
  /* Field: Content: Sponsor level	 */
  $handler->display->display_options['fields']['field_job_level']['id'] = 'field_job_level';
  $handler->display->display_options['fields']['field_job_level']['table'] = 'field_data_field_job_level';
  $handler->display->display_options['fields']['field_job_level']['field'] = 'field_job_level';
  $handler->display->display_options['fields']['field_job_level']['label'] = '';
  $handler->display->display_options['fields']['field_job_level']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_job_level']['element_label_colon'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['preserve_tags'] = '<br>';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '500',
  );
  /* Sort criterion: Content: Sponsor level	 (field_job_level) */
  $handler->display->display_options['sorts']['field_job_level_value']['id'] = 'field_job_level_value';
  $handler->display->display_options['sorts']['field_job_level_value']['table'] = 'field_data_field_job_level';
  $handler->display->display_options['sorts']['field_job_level_value']['field'] = 'field_job_level_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'job_board' => 'job_board',
  );

  /* Display: Job Board */
  $handler = $view->new_display('page', 'Job Board', 'page');
  $handler->display->display_options['path'] = 'job-board';
  $export['job_board'] = $view;

  return $export;
}
