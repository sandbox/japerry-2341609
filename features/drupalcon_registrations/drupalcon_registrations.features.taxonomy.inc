<?php
/**
 * @file
 * drupalcon_registrations.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function drupalcon_registrations_taxonomy_default_vocabularies() {
  return array(
    'sales_discounts' => array(
      'name' => 'Sales + Discounts',
      'machine_name' => 'sales_discounts',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
