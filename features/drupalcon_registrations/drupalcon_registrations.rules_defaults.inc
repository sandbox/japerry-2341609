<?php
/**
 * @file
 * drupalcon_registrations.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function drupalcon_registrations_default_rules_configuration() {
  $items = array();
  $items['commerce_coupon_remove_registrations_from_abandoned_carts'] = entity_import('rules_config', '{ "commerce_coupon_remove_registrations_from_abandoned_carts" : {
      "LABEL" : "Remove registrations from abandoned carts",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "TAGS" : [ "Commerce Coupon" ],
      "REQUIRES" : [ "commerce_cart", "commerce_registration", "commerce_cart_expiration" ],
      "ON" : { "commerce_cart_expiration_cron" : [] },
      "IF" : [
        { "commerce_order_is_cart" : { "commerce_order" : [ "site:current-cart-order" ] } }
      ],
      "DO" : [
        { "commerce_registration_delete_registrations" : { "commerce_order" : [ "site:current-cart-order" ] } }
      ]
    }
  }');
  $items['rules_check_for_existing_registration_and_disable_add_to_cart'] = entity_import('rules_config', '{ "rules_check_for_existing_registration_and_disable_add_to_cart" : {
      "LABEL" : "Check for existing registration and disable add to cart",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "registration" : { "label" : "Registration", "type" : "registration" },
        "line_item" : { "label" : "Line item", "type" : "commerce_line_item" },
        "product_registration_type" : { "label" : "Product\\u0027s registration type", "type" : "text" }
      },
      "IF" : [
        { "data_is" : { "data" : [ "registration:state:active" ], "value" : "1" } },
        { "entity_is_of_type" : { "entity" : [ "registration:entity" ], "type" : "commerce_product" } },
        { "data_is" : { "data" : [ "registration:entity:type" ], "value" : "registration" } },
        { "entity_has_field" : { "entity" : [ "line-item" ], "field" : "commerce_product" } },
        { "data_is" : {
            "data" : [ "line-item:commerce-product:type" ],
            "value" : "registration"
          }
        },
        { "data_is" : {
            "data" : [ "product-registration-type" ],
            "value" : [ "registration:type" ]
          }
        }
      ],
      "DO" : [
        { "drupal_message" : {
            "message" : "You are already registered for this event. You can \\u003Ca href=\\u0022[registration:url]\\u0022\\u003Eview and edit your registration\\u003C\\/a\\u003E.",
            "repeat" : 0
          }
        },
        { "data_set" : { "data" : [ "line-item:commerce-unit-price:amount" ] } }
      ]
    }
  }');
  $items['rules_disallow_ordering_more_than_1_registration'] = entity_import('rules_config', '{ "rules_disallow_ordering_more_than_1_registration" : {
      "LABEL" : "Disallow ordering more than 1 registration",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : { "commerce_order_update" : [] },
      "DO" : [
        { "LOOP" : {
            "USING" : { "list" : [ "commerce-order:commerce-line-items" ] },
            "ITEM" : { "line_item" : "Current line item" },
            "DO" : [
              { "component_rules_limit_quantity_of_registration_line_items" : { "line_item" : [ "line-item" ] } }
            ]
          }
        }
      ]
    }
  }');
  $items['rules_discount_prepaid_order_over_6'] = entity_import('rules_config', '{ "rules_discount_prepaid_order_over_6" : {
      "LABEL" : "Discount prepaid ticket order for quantities over 6",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "REQUIRES" : [
        "commerce_order",
        "rules",
        "commerce_line_item",
        "commerce_product_reference"
      ],
      "ON" : { "commerce_product_calculate_sell_price" : [] },
      "IF" : [
        { "commerce_order_contains_product" : {
            "commerce_order" : [ "commerce-line-item:order" ],
            "product_id" : "dcaus_prepaid_ticket",
            "operator" : "\\u003E=",
            "value" : "6"
          }
        },
        { "entity_has_field" : { "entity" : [ "commerce-line-item" ], "field" : "commerce_product" } },
        { "component_rules_trigger_a_prepaid_ticket_sale" : { "product" : [ "commerce-line-item:commerce-product" ] } }
      ],
      "DO" : [
        { "commerce_line_item_unit_price_multiply" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "amount" : ".8",
            "component_name" : "discount",
            "round_mode" : "2"
          }
        }
      ]
    }
  }');
  $items['rules_email_order_owner_when_invoice_is_paid'] = entity_import('rules_config', '{ "rules_email_order_owner_when_invoice_is_paid" : {
      "LABEL" : "Email order owner when invoice is paid",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "4",
      "OWNER" : "rules",
      "REQUIRES" : [ "commerce_payment", "rules" ],
      "ON" : { "commerce_payment_order_paid_in_full" : [] },
      "IF" : [
        { "commerce_payment_selected_payment_method" : {
            "commerce_order" : [ "commerce_order" ],
            "method_id" : "commerce_cheque"
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[commerce-order:mail-username], accounting@association.drupal.org",
            "subject" : "Payment received for order [commerce-order:order-id]",
            "message" : "We have received your payment for order [commerce-order:order-id]. \\r\\n\\r\\nThank you for your payment! Your order is now COMPLETE.\\r\\n\\r\\nYou can view your purchase history at any time by logging into your account at: [site:login-url] \\r\\n\\r\\nPurchase Information\\r\\nOrder: [commerce-order:order-number]\\r\\nOrder date: [commerce-order:changed]\\r\\nOrder state: Complete\\r\\nOrder total: [commerce-order:commerce_order_total]\\r\\n\\r\\nView or Print this order: [commerce-order:customer-url]\\r\\n\\r\\n- - - - - - - - - - - - - - - - - - - - -\\r\\n\\r\\nRefund + Transfer Policy\\r\\nTickets are non-refundable, however it\\u0027s free and easy to transfer your ticket to another person, if done prior to Friday, 26 September 2014.\\r\\n\\r\\nTo transfer your ticket at no charge, simply contact our Help Desk (amsterdam2014.drupal.org\\/contact) with your transfer request and our awesome support team will help you.\\r\\n\\r\\nPlease contact us if you have any questions about your order at amsterdam2014.drupal.org\\/contact.\\r\\n\\r\\nThis is an automated notification. Please do not reply to this message. ",
            "from" : "accounting@association.drupal.org",
            "language" : [ "commerce-order:state" ]
          }
        }
      ]
    }
  }');
  $items['rules_limit_quantity_of_registration_line_items'] = entity_import('rules_config', '{ "rules_limit_quantity_of_registration_line_items" : {
      "LABEL" : "Limit quantity of registration line items",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "line_item" : { "label" : "Line item", "type" : "commerce_line_item" } },
      "IF" : [
        { "data_is" : { "data" : [ "line-item:quantity" ], "op" : "\\u003E", "value" : "1" } },
        { "entity_has_field" : { "entity" : [ "line-item" ], "field" : "commerce_product" } },
        { "data_is" : {
            "data" : [ "line-item:commerce-product:type" ],
            "value" : "registration"
          }
        }
      ],
      "DO" : [
        { "drupal_message" : {
            "message" : "You can only buy one DrupalCon registration.",
            "type" : "warning"
          }
        },
        { "data_set" : { "data" : [ "line-item:quantity" ], "value" : "1" } }
      ]
    }
  }');
  $items['rules_mark_paid_orders_as_completed'] = entity_import('rules_config', '{ "rules_mark_paid_orders_as_completed" : {
      "LABEL" : "Mark paid orders as completed",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "1",
      "OWNER" : "rules",
      "REQUIRES" : [ "commerce_payment", "commerce_order", "commerce_checkout" ],
      "ON" : { "commerce_checkout_complete" : [] },
      "IF" : [
        { "commerce_payment_order_balance_comparison" : {
            "commerce_order" : [ "commerce-order" ],
            "operator" : "=",
            "value" : "0"
          }
        }
      ],
      "DO" : [
        { "commerce_order_update_state" : { "commerce_order" : [ "commerce-order" ], "order_state" : "completed" } }
      ]
    }
  }');
  $items['rules_redirect_anonymous'] = entity_import('rules_config', '{ "rules_redirect_anonymous" : {
      "LABEL" : "Redirect anonymous",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "10",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "ON" : { "commerce_cart_product_add" : [] },
      "IF" : [
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "1" : "1" } },
            "operation" : "OR"
          }
        }
      ],
      "DO" : [
        { "redirect" : { "url" : "user\\/login", "destination" : 1 } },
        { "drupal_message" : { "message" : "You must be logged in to purchase items." } },
        { "commerce_cart_empty" : { "commerce_order" : [ "commerce-order" ] } }
      ]
    }
  }');
  $items['rules_redirect_to_cart'] = entity_import('rules_config', '{ "rules_redirect_to_cart" : {
      "LABEL" : "Redirect to cart",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "-10",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "ON" : { "commerce_cart_product_add" : [] },
      "DO" : [ { "redirect" : { "url" : "cart" } } ]
    }
  }');
  $items['rules_send_a_prepaid_ticket_notification_admin'] = entity_import('rules_config', '{ "rules_send_a_prepaid_ticket_notification_admin" : {
      "LABEL" : "Send a prepaid ticket notification to order owner (paid)",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "commerce_order", "rules", "commerce_payment" ],
      "ON" : { "commerce_payment_order_paid_in_full" : [] },
      "IF" : [
        { "commerce_order_contains_product" : {
            "commerce_order" : [ "commerce_order" ],
            "product_id" : "dcaus_prepaid_ticket",
            "operator" : "\\u003E=",
            "value" : "1"
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "stephanie@association.drupal.org, [commerce-order:mail]",
            "subject" : "Your prepaid ticket order is processing",
            "message" : "Thanks for your purchase! \\r\\n\\r\\nYou can view your purchase history at any time by logging into your account at: [site:login-url] \\r\\n\\r\\nOrder: [commerce-order:order-number]\\r\\nDate: [commerce-order:changed]\\r\\nOrder status: Complete\\r\\n\\r\\nWe have received your order for one or more prepaid ticket codes. \\r\\nAs these codes are not auto-generated, it may take up to 24 business hours to receive these coupon code. \\r\\n\\r\\nView or Print this order: [commerce-order:customer-url]\\r\\n\\r\\n- - - - - - - - - - - - - - - - - - - - -\\r\\n\\r\\nRefund + Transfer Policy\\r\\nTickets are non-refundable, however it\\u0027s free and easy to transfer your ticket to another person, if done prior to Friday, 26 September 2014.\\r\\n\\r\\nPlease contact us if you have any questions about your order at amsterdam2014.drupal.org\\/contact.\\r\\n\\r\\n\\r\\nThis is an automated notification. Please do not reply to this message. \\r\\n",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_send_a_prepaid_ticket_notification_owner'] = entity_import('rules_config', '{ "rules_send_a_prepaid_ticket_notification_owner" : {
      "LABEL" : "Send a prepaid ticket notification to order owner",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "commerce_order", "commerce_payment", "rules", "commerce_checkout" ],
      "ON" : { "commerce_checkout_complete" : [] },
      "IF" : [
        { "commerce_order_contains_product" : {
            "commerce_order" : [ "commerce_order" ],
            "product_id" : "dcaus_prepaid_ticket",
            "operator" : "\\u003E=",
            "value" : "1"
          }
        },
        { "commerce_payment_selected_payment_method" : {
            "commerce_order" : [ "commerce_order" ],
            "method_id" : "commerce_cheque"
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[commerce-order:mail]",
            "subject" : "Your prepaid ticket order is processing",
            "message" : "Thanks for your purchase! You can view your purchase history at any time by logging into your account at: [site:login-url] \\r\\n\\r\\nOrder: [commerce-order:order-number]\\r\\nDate: [commerce-order:changed]\\r\\nOrder status: UNPAID\\r\\n\\r\\nYou have ordered one or more prepaid tickets for DrupalCon Amsterdam.\\r\\n\\r\\nAs soon as payment it received by our accounting team, we will issue a unique coupon code which your team can use to register for DrupalCon.\\r\\n\\r\\nView or Print this order: [commerce-order:customer-url]\\r\\n\\r\\n- - - - - - - - - - - - - - - - - - - - -\\r\\n\\r\\nRefund + Transfer Policy\\r\\nTickets are non-refundable, however it\\u0027s free and easy to transfer your ticket to another person, if done prior to Friday, 26 September 2014.\\r\\n\\r\\nPlease contact us if you have any questions about your order at amsterdam2014.drupal.org\\/contact.\\r\\n\\r\\n\\r\\nThis is an automated notification. Please do not reply to this message. ",
            "from" : "stephaine@association.drupal.org",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_send_a_registration_notification_e_mail'] = entity_import('rules_config', '{ "rules_send_a_registration_notification_e_mail" : {
      "LABEL" : "Send a registration notification e-mail",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "registration" ],
      "ON" : { "registration_insert" : [] },
      "IF" : [
        { "data_is" : { "data" : [ "site:current-cart-order:status" ], "value" : "completed" } }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[registration:mail], stephanie@association.drupal.org",
            "subject" : "[registration:author]\\tjust registered you for DrupalCon Austin",
            "message" : "Hi [registration:field-badge-first], \\r\\n\\r\\nClear your calendar, because this June, you\\u0027re going to DrupalCon Austin! \\r\\nFrom June 2-6, over 3,500 Drupal geeks will converge on the Austin Convention Center for five days of code, community, and collaboration. \\r\\n\\r\\nView the program: https:\\/\\/austin2014.drupal.org\\/conference\\r\\n\\r\\nConference badge information\\r\\nYour name: [registration:field-badge-first] [registration:field-badge-last]\\t\\r\\nYour job title: [registration:field-badge-job-title]\\r\\nYour username: [registration:user]\\t\\t\\r\\nYour organization: [registration:field-badge-org]]\\t\\r\\nMeal preference: [registration:field-meal-preference]\\r\\nShirt size: [registration:field-shirt-size]\\r\\n\\r\\nOrder information for reference\\r\\nThe person who registered you: [registration:author]\\r\\nOrder number: [registration:commerce_order]\\r\\n\\r\\n\\r\\nIf you need to make any changes to your badge information or if you have any questions, visit our FAQ page at austin2013.drupal.org\\/faq or contact us at austin2013.drupal.org\\/contact \\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n- - -\\r\\nThis e-mail message is an automated notification. Please do not reply to this message. ",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_send_invoice_notification'] = entity_import('rules_config', '{ "rules_send_invoice_notification" : {
      "LABEL" : "Send invoice notification",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "commerce_payment", "rules", "commerce_checkout" ],
      "ON" : { "commerce_checkout_complete" : [] },
      "IF" : [
        { "commerce_payment_selected_payment_method" : {
            "commerce_order" : [ "commerce_order" ],
            "method_id" : "commerce_cheque"
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[commerce-order:mail-username], accounting@association.drupal.org",
            "subject" : "DrupalCon Amsterdam payment instructions for order [commerce-order:order-id]",
            "message" : "You have created an order with us!\\r\\n\\r\\nYou can view your order history at any time by logging into your account at: [site:login-url] \\r\\n\\r\\nOrder [commerce-order:order-id]\\t\\r\\nTotal: [commerce-order:commerce_order_total]\\r\\nStatus: PENDING \\r\\n\\r\\nPlease note your order is set to Pending (awaiting payment) and will remain so until payment is received in full. If you are registering for a time-sensitive item, such as an earlybird ticket, payment must be received in advance of the deadline for the special price to apply. \\r\\n\\r\\nHow to submit payment to the Drupal Association:\\r\\nDrupalCon, Inc.\\r\\nBank: KBC Bank NV - Havenlaan 2 - 1080 Brussels, Belgium \\r\\nIBAN: BE35 7310 2258 7837\\r\\nBIC: KREDBEBB\\r\\n\\r\\nYou will receive an order completion email once your payment is processed. If you do not hear back within 3-4 days of submitting payment, please contact us at amsterdam2014.drupal.org\\/contact. \\r\\n\\r\\n- - - - - - - - - - - - - - - - - - - - -\\r\\n\\r\\nRefund + Transfer Policy\\r\\nTickets are non-refundable, however it\\u0027s free and easy to transfer your ticket to another person, if done prior to Friday, 26 September 2014.\\r\\n\\r\\nPlease contact us if you have any questions about your order at amsterdam2014.drupal.org\\/contact.\\r\\n\\r\\n\\r\\nThis is an automated notification. Please do not reply to this message. \\r\\n",
            "from" : "accounting@association.drupal.org",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_set_the_coupon_invalid_if_max_uses_of_coupon_is_reached'] = entity_import('rules_config', '{ "rules_set_the_coupon_invalid_if_max_uses_of_coupon_is_reached" : {
      "LABEL" : "Set the coupon invalid if max uses of coupon is reached",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "commerce_coupon" ],
      "USES VARIABLES" : {
        "commerce_coupon" : { "label" : "Coupon", "type" : "commerce_coupon" },
        "number_of_uses" : { "label" : "Number of uses of the coupon", "type" : "integer" }
      },
      "IF" : [
        { "data_is" : { "data" : [ "number-of-uses" ], "op" : "\\u003E", "value" : "1" } }
      ],
      "DO" : [
        { "drupal_message" : {
            "message" : "Sorry, the maximum number of coupons for this order has been reached.",
            "type" : "error"
          }
        },
        { "commerce_coupon_action_is_invalid_coupon" : [] }
      ]
    }
  }');
  $items['rules_trigger_a_prepaid_ticket_sale'] = entity_import('rules_config', '{ "rules_trigger_a_prepaid_ticket_sale" : {
      "LABEL" : "Trigger a prepaid ticket product sale",
      "PLUGIN" : "and",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "product" : { "label" : "Product", "type" : "commerce_product" } },
      "AND" : [
        { "entity_has_field" : { "entity" : [ "product" ], "field" : "field_product_sales_discounts" } },
        { "NOT data_is_empty" : { "data" : [ "product:field-product-sales-discounts" ] } },
        { "NOT data_is_empty" : { "data" : [ "product:field-product-sales-discounts:tid" ] } },
        { "data_is" : {
            "data" : [ "product:field-product-sales-discounts:tid" ],
            "value" : "6"
          }
        }
      ]
    }
  }');
  $items['rules_trigger_a_product_sale'] = entity_import('rules_config', '{ "rules_trigger_a_product_sale" : {
      "LABEL" : "Trigger a training product sale ",
      "PLUGIN" : "and",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "product" : { "label" : "Product", "type" : "commerce_product" } },
      "AND" : [
        { "entity_has_field" : { "entity" : [ "product" ], "field" : "field_product_sales_discounts" } },
        { "NOT data_is_empty" : { "data" : [ "product:field-product-sales-discounts" ] } },
        { "NOT data_is_empty" : { "data" : [ "product:field-product-sales-discounts:tid" ] } },
        { "data_is" : {
            "data" : [ "product:field-product-sales-discounts:tid" ],
            "value" : "7"
          }
        }
      ]
    }
  }');
  return $items;
}
