<?php
/**
 * @file
 * drupalcon_registrations.features.inc
 */

/**
 * Implements hook_commerce_product_default_types().
 */
function drupalcon_registrations_commerce_product_default_types() {
  $items = array(
    'registration' => array(
      'type' => 'registration',
      'name' => 'Registration',
      'description' => 'Use this product type to set up all ticket\\registration products for DrupalCon (conference registration, exhibit hall, trainings, etc)',
      'help' => '',
      'revision' => '1',
    ),
  );
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function drupalcon_registrations_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function drupalcon_registrations_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function drupalcon_registrations_node_info() {
  $items = array(
    'ticket_sale_page' => array(
      'name' => t('Ticket Sale Page'),
      'base' => 'node_content',
      'description' => t('Use this content type to set up the sale of registration products (conference registration, exhibit hall, trainings, etc)'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'training' => array(
      'name' => t('Training'),
      'base' => 'node_content',
      'description' => t('Used for the Training content type. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implements hook_default_registration_type().
 */
function drupalcon_registrations_default_registration_type() {
  $items = array();
  $items['conference_registration'] = entity_import('registration_type', '{
    "name" : "conference_registration",
    "label" : "Conference Registration",
    "locked" : "0",
    "weight" : "0",
    "data" : null
  }');
  $items['drupal_association_membership'] = entity_import('registration_type', '{
    "name" : "drupal_association_membership",
    "label" : "Drupal Association Membership",
    "locked" : "0",
    "weight" : "0",
    "data" : null
  }');
  $items['exhibit_hall_registration'] = entity_import('registration_type', '{
    "name" : "exhibit_hall_registration",
    "label" : "Single Day Registration",
    "locked" : "0",
    "weight" : "0",
    "data" : null
  }');
  $items['non_conference_ticket'] = entity_import('registration_type', '{
    "name" : "non_conference_ticket",
    "label" : "CXO Registration",
    "locked" : "0",
    "weight" : "0",
    "data" : null
  }');
  $items['training_registration'] = entity_import('registration_type', '{
    "name" : "training_registration",
    "label" : "Training",
    "locked" : "0",
    "weight" : "0",
    "data" : null
  }');
  return $items;
}
