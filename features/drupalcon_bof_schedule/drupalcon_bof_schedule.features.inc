<?php
/**
 * @file
 * drupalcon_bof_schedule.features.inc
 */

/**
 * Implements hook_views_api().
 */
function drupalcon_bof_schedule_views_api() {
  return array("version" => "3.0");
}
