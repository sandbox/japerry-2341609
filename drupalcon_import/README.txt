Example JSON feed from association.drupal.org/api/association_product_buyers.json

{
  "orders" : [
    {
      "order" : {
        "product_title" : "DrupalCon Denver 2012 registration",
        "product_nid" : "1484",
        "user_name" : "coltrane",
        "user_local_uid" : "3897",
        "profile_last_name" : "Jeavons",
        "profile_first_name" : "Ben",
        "profile_full_name" : "Ben Jeavons",
        "profile_job" : "Makes it Awesome"
      }
    },
  ]
}
