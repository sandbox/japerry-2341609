<?php

function drupalcon_import_drush_command() {
  $items['drupalcon-import-registrations'] = array(
    'description' => 'Import registrations',
    'aliases' => array('drupalcon-regs'),
  );
  return $items;
}

function drush_drupalcon_import_registrations() {
  drupalcon_import_process_registrations();
}
