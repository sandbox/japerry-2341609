<?php

/**
 * Implements hook_default_rules_configuration_alter().
 */
function drupalorg_drupalcon_default_rules_configuration_alter(&$configs) {
  // Only make payment methods available when balance is > 0. See
  // http://www.drupalcommerce.org/faq/does-drupal-commerce-support-free-orders
  foreach (commerce_payment_methods() as $method_id => $payment_method) {
    if (isset($configs['commerce_payment_' . $method_id])) {
      $configs['commerce_payment_' . $method_id]->condition('commerce_payment_order_balance_comparison', array(
        'commerce_order:select' => 'commerce-order',
        'operator' => '>',
        'value' => '0',
      ));
    }
  }

  // On order completion, set registrations to complete instead of pending.
  if (isset($configs['commerce_registration_pending_upon_order_completion'])) {
    $configs['commerce_registration_pending_upon_order_completion']->label = t('Set Registrations to Complete upon Order Completion');
    foreach ($configs['commerce_registration_pending_upon_order_completion']->actions() as $action) {
      if ($action->getElementName() === 'commerce_registration_set_state') {
        $action->settings['registration_state'] = array('complete' => 'complete');
      }
    }
  }
}
